import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Demo extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Create a button
        Button button = new Button("Click Me");

        // Create a layout and add the button to it
        StackPane root = new StackPane();
        root.getChildren().add(button);

        // Create a scene and set the layout
        Scene scene = new Scene(root, 600, 500);

        // Set the scene in the stage and show the stage
        primaryStage.setScene(scene);
        primaryStage.setTitle("JavaFX Button Example");
        primaryStage.show();
    }
}
